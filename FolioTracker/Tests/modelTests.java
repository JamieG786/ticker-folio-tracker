import model.*;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;


public class modelTests {

    @Test
    public void newFolioTest(){
        Model m = new Model();
        m.addFolio("test");
        Folio f = m.getCurrentFolio();
        assertEquals(f.getName(), "test");
    }

    @Test
    public void sameNameFolio(){
        Model m = new Model();
        m.addFolio("test");
        Exception e = assertThrows(IllegalArgumentException.class,
                ()->m.addFolio("test"));
        assertEquals("Folio with this name already exists", e.getMessage());
    }

    @Test
    public void switchFolioTest(){
        Model m = new Model();
        m.addFolio("test1");
        m.addFolio("test2");
        //Switch current folio from test2 (current) to test1.
        m.switchCurrentFolio("test1");
        Folio f = m.getCurrentFolio();
        assertEquals(f.getName(), "test1");
    }

    @Test
    public void switchToNonFolioTest(){
        Model m = new Model();
        Exception e = assertThrows(IllegalArgumentException.class, ()->m.switchCurrentFolio("test1"));
        assertEquals("Folio does not exist", e.getMessage());
    }

    @Test
    public void getFolioNameTest(){
        Model m = new Model();
        m.addFolio("test");
        Folio f = m.getCurrentFolio();
        assertEquals(f.getName(), "test");
    }



    @Test
    public void removeFolioTest(){
        Model m = new Model();
        m.addFolio("test");
        Folio f = m.getCurrentFolio();
        m.removeFolio(f);
        Exception e = assertThrows(IllegalArgumentException.class, ()->m.switchCurrentFolio("test"));
        assertEquals("Folio does not exist", e.getMessage());
    }

    @Test
    public void getStocksTest() throws NoSuchTickerException, WebsiteDataException {
        Model m = new Model();
        m.addFolio("test");
        Folio f = m.getCurrentFolio();
        f.addStock("ABC", "test", 100);
        assertEquals(m.getStocks(f),f.getStocks());
    }

    @Test
    public void addSharesTest() throws NoSuchTickerException, WebsiteDataException {
        Model m = new Model();
        m.addFolio("test");
        Folio f = m.getCurrentFolio();
        f.addStock("ABC" ,"test", 100);
        m.addShares(f, "test", 50);
        assertEquals(f.getShares("test"), 150);
    }

    @Test
    public void removeSharesTest() throws NoSuchTickerException, WebsiteDataException {
        Model m = new Model();
        m.addFolio("test");
        Folio f = m.getCurrentFolio();
        f.addStock("ABC", "test", 100);
        m.removeShares(f, "test", 50);
        assertEquals(f.getShares("test"), 50);
    }
}
