import model.TableFormattedItem;
import model.*;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class FolioTests {

    @Test
    public void createFolioTest() {
        Folio f = new Folio("Test");
        assertEquals(f.getName(), "Test");
    }

    @Test
    public void changeNameTest() {
        Folio f = new Folio("Test");
        f.setName("Folio1");
        assertEquals(f.getName(), "Folio1");
    }

    @Test
    public void addStockTest() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        assertNotEquals(f.getStocks(), "{}");
    }

    @Test
    public void addSameNameStockTest() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        Exception e=assertThrows(IllegalArgumentException.class,
                ()->f.addStock("TXT", "stockTest", 100));
        assertEquals("Share with this name already exists",e.getMessage());
    }

    @Test
    public void removeStockTest() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        HashMap<String, Stock> map = new HashMap<>();
        List<TableFormattedItem> stocks = new ArrayList<>(f.getStocks().values());
        TableFormattedItem s = stocks.get(0);
        assertNotEquals(f.getStocks(), map);
        f.removeStock(s);
        assertEquals(f.getStocks(), map);
    }

    @Test
    public void increaseSharesTest() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        assertEquals(f.getShares("stockTest"), 100);
        f.increaseShares("stockTest", 500);
        assertEquals(f.getShares("stockTest"), 600);
    }

    @Test
    public void decreaseSharesTestException() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        Exception e= assertThrows(IllegalArgumentException.class,
                ()->f.decreaseShares("stockTest", 500));
        assertEquals("You have less shares than you are trying to remove", e.getMessage());
        assertEquals(f.getShares("stockTest"), 100);
    }

    @Test
    public void decreaseSharesTest() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        assertDoesNotThrow(()->f.decreaseShares("stockTest", 50));
        assertEquals(f.getShares("stockTest"), 50);
    }

    @Test
    public void saveToStorageTest() throws NoSuchTickerException, WebsiteDataException, IOException, ClassNotFoundException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        f.saveToStorage("test.ser");
        FileInputStream in = new FileInputStream("test.ser");
        ObjectInputStream stream = new ObjectInputStream(in);
        Folio f2 = new Folio("");
        f2.readExternal(stream);
        stream.close();
        assertEquals(f.getName(),f2.getName());
    }

    @Test
    public void addWatchTest() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT","stockTest",100);
        f.addWatch("watchTest", "stockTest", 1.0, 3, 1);
        List<IWatch> watches = f.getWatches();
        IWatch watch = watches.get(0);
        assertEquals(watch.getName(),"watchTest");
    }

    @Test
    public void addSameNameWatchTest() throws NoSuchTickerException, WebsiteDataException {
        Folio f = new Folio("Test");
        f.addStock("TXT", "stockTest", 100);
        f.addWatch("watchTest", "stockTest", 1.0, 3, 1);
        Exception e = assertThrows(IllegalArgumentException.class, ()->f.addWatch("watchTest", "stockTest", 1.0, 3, 1));
        assertEquals("A watch with this name already exists", e.getMessage());
    }

    @Test
    public void noStockFoundTest(){
        Folio f = new Folio("Test");
        Exception e = assertThrows(IllegalArgumentException.class, ()->f.addWatch("watchTest", "stockTest", 1.0, 3, 1));
        assertEquals("A stock with this name doesnt exist", e.getMessage());
    }
}
