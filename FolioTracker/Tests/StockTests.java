import model.NoSuchTickerException;
import model.StrathQuoteServer;
import model.WebsiteDataException;
import model.Stock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class StockTests {

    @Test
    public void createStockTest() throws WebsiteDataException, NoSuchTickerException {
        Stock s = new Stock("TXT", "Test", 500);
        assertEquals(s.getShortName(), "TXT");
        assertEquals(s.getLongName(), "Test");
        assertEquals(s.getQuantity(), 500);
    }

    @Test
    public void updatePriceTest() throws WebsiteDataException, NoSuchTickerException {
        Stock s = new Stock("TXT", "Test", 1000);
        assertNotEquals(s.getNewValue(), null);
        StrathQuoteServer server=mock(StrathQuoteServer.class);
        when(server.getLastValue(s.getShortName())).thenReturn("500");
        s.updatePrice(server);
        assertEquals(s.getNewValue(), 500);
    }

    @Test
    public void changeSymbolTest() throws WebsiteDataException, NoSuchTickerException {
        Stock s = new Stock("TXT", "Test", 1000);
        String firstSymbol = s.getShortName();
        String firstName = s.getLongName();
        double firstPrice = s.getNewValue();
        s.setSymbol("ASP");
        String secondSymbol = s.getShortName();
        String secondName = s.getLongName();
        double secondPrice = s.getNewValue();

        assertNotEquals(firstSymbol, secondSymbol);
        assertEquals(firstName, secondName);
        assertNotEquals(firstPrice, secondPrice);
    }

    @Test
    public void setNameTest() throws WebsiteDataException, NoSuchTickerException {
        Stock s = new Stock("ASP", "Test", 1000);
        assertEquals(s.getLongName(), "Test");
        s.setLongName("Changed");
        assertEquals(s.getLongName(), "Changed");
    }

    @Test
    public void SharesTest() throws WebsiteDataException, NoSuchTickerException {
        Stock s = new Stock("ASP", "Test", 1000);
        assertEquals(s.getQuantity(), 1000);
        s.setQuantity(500);
        assertEquals(s.getQuantity(), 500);
    }

    @Test
    public void ProfitTest() throws WebsiteDataException, NoSuchTickerException {
        Stock s = new Stock("ASP", "Test", 10);
        s.setOldValue(12);
        StrathQuoteServer server=mock(StrathQuoteServer.class);
        when(server.getLastValue(s.getShortName())).thenReturn("500");
        s.updatePrice(server);
        assertEquals(4880,s.getProfit());
    }
}
