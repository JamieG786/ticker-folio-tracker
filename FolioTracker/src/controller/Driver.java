package controller;

/**
 * This is the driver class. Its sole purpose is to launch the controller.
 */
public class Driver {
    public static void main(String[] args) {
        Controller.launch(Controller.class);
    }
}
