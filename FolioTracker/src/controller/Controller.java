package controller;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;
import javafx.stage.Stage;
import model.*;
import view.CreateFolio;
import view.EditStockMenu;
import view.View;
import view.WatchesView;
import view.components.TableTab;

import java.io.IOException;
/**
 * This is the class to control the overall running of the program.It contains methods to start the program and add actions to the UI buttons. Creating folios and adding/editing/removing stock is also done here.
 */
public class Controller extends Application implements Runnable {
    private Model model;
    private View view;
    private CreateFolio createFolio;

    public Controller() {
        this.model = new Model();
    }


    /**
     * This method will start the UI and start the actions of the buttons
     *
     *
     * @param s required parameter for Runnable implementation
     * @throws WebsiteDataException is thrown if the server could not be reached to update the stock.
     * @throws NoSuchTickerException is thrown if the ticker for the entered folio does not existed.
     */
    public void start(Stage s) {
        this.view = new View();
        this.createFolio = new CreateFolio();
        this.buttonBuilder();

        view.show();
        new Thread(this).start();
    }


    /**
     * This is method adds the actions to each button on the UI (including the drop down menu bar).
     */
    private void buttonBuilder() {
        // Make sure the program closes completely
        view.setOnCloseRequest(e -> System.exit(0));

        // Menu Items
        view.getMenuBar().getExit().setOnAction(e -> System.exit(0));
        view.getMenuBar().getNewPort().setOnAction(e -> {
            createFolio.reset();
            createFolio.showFolioScene();
        });
        createFolio.getCreate().setOnAction(e -> {
            model.addFolio(createFolio.getName());
            createFolio.hideFolioScene();
            model.switchCurrentFolio(createFolio.getName());
            this.createFolio(model.getCurrentFolio());
        });

        view.getMenuBar().getSavePort().setOnAction( e -> {
            if (view.getTablesPane().getTabs().size() > 0) {
                try {
                    String path = view.getFilePath(View.FileAccess.SAVE, model.getCurrentFolio().getName());
                    System.out.println(path);
                    model.getCurrentFolio().saveToStorage(path);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    view.displayBasicError("Could not save file");
                }
            } else{
                view.displayBasicError("Nothing to save");
            }
        });
        view.getMenuBar().getLoadPort().setOnAction( e -> {
            try {
                String path = view.getFilePath(View.FileAccess.LOAD, "");
                this.createFolio(Folio.loadFromStorage(path));
            } catch (NullPointerException ex) {
                System.out.println("Cancelled?");
            } catch (IOException | ClassNotFoundException ex) {
                view.displayBasicError("Error loading file");
            }
        });

        createFolio.getCreate().setOnAction(createFolioUI());

        // Action Bar
        view.getActionBar().getRefreshButtonBox().getRefreshButton().setOnAction(new RefreshEvent(model, view));
        view.getActionBar().getRefreshButtonBox().getEditWatchesButton().setOnAction(launchWatchesView());

        view.getActionBar().getStockModifierBox().getAddButton().setOnAction(addStock());
        view.getActionBar().getStockModifierBox().getEditButton().setOnAction(editStock());
        view.getActionBar().getStockModifierBox().getRemoveButton().setOnAction(removeStock());
    }

    /**
     * This method creates a new folio.
     * @param f new folio to be created
     */
    private void createFolio(Folio f) {
        model.switchCurrentFolio(f.getName());
        view.getTablesPane().createFolioTab(f);
        view.getTablesPane().getSelectionModel().select(view.getTablesPane().getTab(f.getName()));


        view.getTablesPane().getSelectionModel().getSelectedItem().setOnCloseRequest(e -> {
           model.removeFolio(model.getCurrentFolio());
        });
        view.getTablesPane().getSelectionModel().selectedItemProperty().addListener(
                (observableValue, tab, t1) -> {
                    if (view.getTablesPane().getTabs().size() > 0) {
                        String name = view.getTablesPane().getSelectionModel().getSelectedItem().getText();
                        model.switchCurrentFolio(name);
                        view.getStatusBar().getPortVal().setText(Double.toString(model.getFolioValue(model.getCurrentFolio())));
                    } else {
                        view.getStatusBar().getPortVal().setText("0");
                    }
                }
        );
    }


    /**
     * This method keeps the UI refreshing after a set time.
     */
    public void run() {
        while (true) {
            Platform.runLater(new RefreshEvent(model, view));
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * This method creates a new folio when the option is selected in the drop down menu.
     */
    private EventHandler<ActionEvent> createFolioUI() {
        return actionEvent -> {

            try {
                String name = createFolio.getName();
                if (!name.equals("")) {
                    model.addFolio(name);
                    createFolio.hideFolioScene();
                    model.switchCurrentFolio(createFolio.getName());
                    this.createFolio(model.getCurrentFolio());
                } else {
                    view.displayBasicError("The folio name cannot be empty");
                }
            } catch (IllegalArgumentException ex) {
                view.displayBasicError(ex.getMessage());
            }

        };
    }


    /**
     * This method adds a new stock to the selected folio when the add stock button is pressed.
     */
    private EventHandler<ActionEvent> addStock() {
        return actionEvent -> {
            Folio f = model.getCurrentFolio();
            String name = view.getActionBar().getStockModifierBox().getNameText().getText().stripTrailing();
            String ticker = view.getActionBar().getStockModifierBox().getTickerText().getText().stripTrailing();
            int quantity = -1;

            try {
                // Guarantee that the quantity is numeric
                quantity = Integer.parseInt(view.getActionBar().getStockModifierBox().getQuantityText().getText());

                if (f != null) {


                    if (!name.equals("") && !ticker.equals("")) {
                        // Add stock to folio
                        if (quantity >= 0) {
                            f.addStock(ticker, name, quantity);
                            view.getActionBar().getStockModifierBox().emptyFields();
                            new RefreshEvent(model, view).tableRefresh((TableTab) view.getTablesPane().getSelectionModel().getSelectedItem());
                        } else {
                            view.displayBasicError("Please enter a positive stock quantity");
                        }
                    } else {
                        view.displayBasicError("Make sure you do not leave entry boxes blank");
                    }
                } else {
                    view.displayBasicError("You do not have a folio currently open");
                }
            } catch (NumberFormatException ex) {
                view.displayBasicError("Only use numeric values for the quantity.");
            } catch (WebsiteDataException ex) {
                System.out.println(ex);
                view.displayBasicError("Could not connect to server. Please check your internet connection");
            } catch (NoSuchTickerException ex) {
                System.out.println(ex);
                view.displayBasicError("Could not find ticker : " + ticker);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex);
                view.displayBasicError("Ticker already exists in folio");
            }
        };
    }

    /**
     * This method adds a watch to the selected folio when the add watch button is pressed.
     */
    private EventHandler<ActionEvent> addWatch(WatchesView v) {
        return actionEvent -> {
            Folio f = model.getCurrentFolio();
            String name = v.getWatchAdderBox().getNameText().getText().stripTrailing();
            String stockName = v.getWatchAdderBox().getStockText().getText().stripTrailing();
            int amount = -1;
            double price=-1;

            try {
                amount = Integer.parseInt(v.getWatchAdderBox().getAmountText().getText());
                price = Double.parseDouble(v.getWatchAdderBox().getPriceText().getText());
                int action;
                if (v.getWatchAdderBox().getChoiceBox().getValue().equals("Sell")){
                    action=IWatch.SELL;
                }
                else{
                    action= IWatch.BUY;
                }

                if (!name.equals("") && !stockName.equals("")) {
                    if (amount>=0){
                        if(price>=0){
                            f.addWatch(name,stockName,price,amount,action);
                            v.getWatchAdderBox().emptyFields();
                            v.getWatchTable().refresh();
                        }
                        else {
                            view.displayBasicError("Please enter a positive price");
                        }
                    }
                    else {
                        view.displayBasicError("Please enter a positive stock amount to use");
                    }
                } else {
                    view.displayBasicError("Make sure you do not leave entry boxes blank");
                }
            } catch (NumberFormatException ex) {
                view.displayBasicError("Make sure stock amount and price are valid numbers");
            } catch (IllegalArgumentException ex) {
                view.displayBasicError(ex.getMessage());
            }
        };
    }

    /**
     * This method launches the watch editing window when the edit watches button is pressed.
     */
    private EventHandler<ActionEvent> launchWatchesView() {
        return actionEvent -> {
            try{
                WatchesView v= new WatchesView(model.getCurrentFolio().getWatchData());
                v.getWatchAdderBox().getAddButton().setOnAction(addWatch(v));
                v.getWatchAdderBox().getRemoveButton().setOnAction(removeWatch(v));

                v.showAndWait();
            } catch (NullPointerException e){
                view.displayBasicError("Please make sure you have a folio loaded");
            }

        };
    }

    /**
     * This method removes the selected watch when the remove watch button is pressed.
     */
    private EventHandler<ActionEvent> removeWatch(WatchesView v) {
        return actionEvent -> {

            try {
                IWatch w = v.getWatchTable().getSelectionModel().getSelectedItem();
                boolean removeStock = view.displayWatchRemoveDialog(w.getName());

                if (removeStock) {
                    model.getCurrentFolio().removeWatch(w.getName());
                }
            } catch (NullPointerException e) {
                System.out.println(e);
                view.displayBasicError("Make sure you have a folio open and you have a watch selected");
            }
        };
    }

    /**
     * This method edits the selected stock when the edit stock button is pressed.
     */
    private EventHandler<ActionEvent> editStock() {
        return actionEvent -> {
            try {
                TableTab t = (TableTab) view.getTablesPane().getSelectionModel().getSelectedItem();
                String shareName = t.getSelectedShare().getLongName();

                // Create pop up edit menu for selected stock
                EditStockMenu e = new EditStockMenu(t.getSelectedShare());
                e.getCancelButton().setOnAction(v -> e.close());
                e.getAcceptButton().setOnAction(v -> {
                    int amount = -1, newAmount = -1;
                    double newPrice = -1;

                    amount = this.model.getCurrentFolio().getShares(shareName);

                    try {
                        newPrice = Double.parseDouble(e.getPurchaseVal().getText());

                        newAmount = Integer.parseInt(e.getQuantityText().getText());
                    } catch (NumberFormatException ex) {
                        view.displayBasicError("Please use a valid numeric value for all fields");
                    }

                    // Input validation
                    if (newAmount > 0 && newPrice > 0) {
                        if (amount > newAmount) {
                            this.model.getCurrentFolio().decreaseShares(shareName, amount - newAmount);
                        } else {
                            this.model.getCurrentFolio().increaseShares(shareName, newAmount - amount);
                        }
                        this.model.getCurrentFolio().getStocks().get(shareName).setOldValue(newPrice);
                    } else {
                        view.displayBasicError("Please make sure both values are greater than 0");
                    }
                    if (!e.getNameText().getText().equals(shareName)) {
                        if (e.getNameText().getLength() > 0) {
                            try {
                                this.model.getCurrentFolio().changeStockName(shareName, e.getNameText().getText());
                            } catch (IllegalArgumentException ex) {
                                view.displayBasicError("Please make sure that this name does not exist in the folio. Name was not changed.");
                            }
                        } else {
                            view.displayBasicError("Please make sure that the new name is given at least 1 character. Name was not changed.");
                        }
                    }
                    e.close();
                    new RefreshEvent(model, view).tableRefresh((TableTab)view.getTablesPane().getSelectionModel().getSelectedItem());
                });

                e.showAndWait();
            } catch (NullPointerException e) {
                System.out.println(e);
                view.displayBasicError("Please select a row from the table");
            }

        };

    }
    /**
     * This method removes the selected stock when the remove stock button is pressed.
     */
    private EventHandler<ActionEvent> removeStock() {
        return actionEvent -> {

            try {
                TableTab tableTab = (TableTab) view.getTablesPane().getSelectionModel().getSelectedItem();
                TableFormattedItem t = tableTab.getSelectedShare();
                boolean removeStock = view.displayStockRemoveDialog(t.getLongName());

                if (removeStock) {
                    model.getCurrentFolio().removeStock(t);
                    for (Tab tab:view.getTablesPane().getTabs()){
                        ((TableTab)tab).getTable().refresh(); // stops javafx bug in cell rendering on data update
                    }
                }
            } catch (NullPointerException e) {
                System.out.println(e);
                view.displayBasicError("Make sure you have a folio open and you have a stock selected");
            }
        };

    }

}