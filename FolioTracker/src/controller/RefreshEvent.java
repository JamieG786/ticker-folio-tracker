package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;
import model.Model;
import model.NoSuchTickerException;
import model.WebsiteDataException;
import view.View;
import view.components.TableTab;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 * This is the class for the refreshing of the UI. This will we used by the controller buttons and the refresh button to update and display the latest values.
 */
public class RefreshEvent implements EventHandler<ActionEvent>, Runnable {

    private Model model;
    private View view;


    public RefreshEvent(Model model, View stage) {
        this.model = model;
        this.view = stage;
    }

    /**
     * This method is for handling when the refresh button is pressed on the UI.
     */
    public void handle(ActionEvent actionEvent) {
        System.out.println("Manual Refresh");
        this.priceRefresh();
    }

    /**
     * This method is for creating and running a new refresh object and starting the refreshing of prices.
     */
    public void run() {
        System.out.println("Automatic Refresh");
        System.out.println("New Refresh");
        new RefreshEvent(model, view).priceRefresh();
    }

    /**
     * This will update the table to display the latest values.It then updates the status bar with the new total price and overall profit
     * @param t the table in the UI to update
     */
    public void tableRefresh(TableTab t) {
        t.getTable().refresh();
        view.getStatusBar().getPortVal().setText(Double.toString(model.getFolioValue(model.getCurrentFolio())));
        view.getStatusBar().getOverallProfit().setText(Double.toString(model.currentFolioProfit(model.getCurrentFolio())));
    }


    /**
     * This will update the prices of each stock and call tableRefresh to display new values.
     * @throws WebsiteDataException is thrown if the server could not be reached to update the stock.
     * @throws NoSuchTickerException is thrown if the ticker for the entered folio does not existed.
     */
    public void priceRefresh() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");

        try {
            model.refreshStocks();
            String newTime = LocalDateTime.now().format(format);

            for (Tab t : view.getTablesPane().getTabs()) {
                tableRefresh((TableTab) t);
            }
            view.getStatusBar().getLastRefresh().setText(newTime);
        }
        catch (WebsiteDataException e) {
            view.displayBasicError("Could not connect to server. Please check your internet connection");
        } catch (NoSuchTickerException e) {
            view.displayBasicError("The server does not a stock symbol. Please verify all symbols are correct");
        } catch (IllegalStateException e){
            view.displayBasicError(e.getMessage());
            priceRefresh(); //complete refresh after notification is displayed
        }
    }







}