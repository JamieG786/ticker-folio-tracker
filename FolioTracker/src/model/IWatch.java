package model;

public interface IWatch {
    int SELL = 1;
    int BUY = 0;

    double getTriggerValue();

    int getAmount();

    int getAction();

    Stock getTarget();

    String getName();
}
