package model;

public interface UpdateStockInterface {
    void updatePrice(StrathQuoteServer server) throws NoSuchTickerException, WebsiteDataException;
}
