package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;



/**
 * This is the class for the managing a folio
 * All the stocks within a particular folio will be stored within a hashmap(Name, Object)
 */

public class Folio implements Externalizable {

    private String name;
    private HashMap<String, TableFormattedItem> stocks = new HashMap<>();
    private static final long serialVersionUID=29L;
    private ObservableList<TableFormattedItem> tableData;
    private List<IWatch> watches;
    private ObservableList<IWatch> watchData; //exist because javafx observable list wrappers are bugged

    /**
     * Creates a new folio with a name
     * @param folName Name of the new folio
     */
    public Folio(String folName) {
        watches=new LinkedList<>();
        name = folName;
        watchData = FXCollections.observableArrayList();
        tableData = FXCollections.observableArrayList();
    }

    public List<IWatch> getWatches() {
        return watches;
    }

    /**
     * This method adds a watch to be monitored by the program
     * @param name of the watch
     * @param targetName of the stock
     * @param triggerValue The value which the watch will boy/sell stocks at
     * @param amount of stocks to buy/sell
     * @param action of whether the folio is to buy or sell stocks
     */
    public void addWatch(String name, String targetName, double triggerValue, int amount , int action){
        for(int i=0;i<watches.size();i++){
            if(watches.get(i).getName().equals(name)){
                throw new IllegalArgumentException("A watch with this name already exists");
            }
        }
        if (!stocks.containsKey(targetName)){
            throw new IllegalArgumentException("A stock with this name doesnt exist");
        }
        IWatch a =new Watch(name, (Stock)stocks.get(targetName),triggerValue,amount,action);
        watches.add(a);
        watchData.add(a);
    }

    /**
     * This method checks if an individual watch has hit its target price.
     * If it has(triggered the watch) it will change the quantity depending on the action and return true else false.
     */
    public boolean checkWatch(IWatch w) throws IllegalStateException{
        Stock s=w.getTarget();
        if(w.getAction()==IWatch.BUY&&w.getTriggerValue()>=s.getNewValue()){
            s.setQuantity(s.getQuantity()+w.getAmount());
            return true;
        }
        if(w.getAction()==IWatch.SELL&&w.getTriggerValue()<=s.getNewValue()){
            if (s.getQuantity()<w.getAmount()){
                int q=s.getQuantity();
                s.setQuantity(0);
                throw new IllegalStateException("Watch "+w.getName()
                        +" tried to sell more than was available \n only managed to sell "
                        + q);
            }
            else{
                s.setQuantity(s.getQuantity()-w.getAmount());
                return true;
            }
        }
        return false;
    }

    /**
     * This method runs through all of the watches and checks if they have been triggered.
     * @throws IllegalStateException
     */
    public void checkWatches() throws IllegalStateException{
        StringBuilder failedWatches= new StringBuilder();
        for (int i=0;i<watches.size();i++){
            try {
                boolean triggered = checkWatch(watches.get(i));
                if (triggered){
                    watches.remove(i);
                    watchData.remove(i);
                    i--;
                }
            } catch (IllegalStateException e){
                watches.remove(i);
                watchData.remove(i);
                failedWatches.append("\n").append(e.getMessage());
                i--;
            }
        }
        if(!failedWatches.toString().equals("")){
            throw new IllegalStateException(failedWatches.toString());
        }
    }

    /**
     * This method removes a watch from the folio.
     * @param name of the watch
     */
    public void removeWatch(String name){
        for(int i=0;i<watches.size();i++){
            if(watches.get(i).getName().equals(name)){
                watches.remove(i);
                watchData.remove(i);
                return;
            }
        }
    }

    /**
     * This method takes a stock from the user
     * <p>
     * It checks if the name is unique, throws an error otherwise, then creates a stock object and inserts it into the hashmap
     * </p>
     * @param symbol of the stock(ticker)
     * @param name of the stock
     * @param noOfShares the user has bought
     * @throws WebsiteDataException if there is an issue getting the data from the website
     * @throws IllegalArgumentException if the share name isnt unique
     * @throws NoSuchTickerException if the ticker symbol they have entered doesnt match a stock
     */
    public void addStock(String symbol, String name, int noOfShares) throws WebsiteDataException,
            IllegalArgumentException, NoSuchTickerException {
        System.out.println(name + " " + symbol + " " + noOfShares);
        Stock s = new Stock(symbol, name, noOfShares);
        if(stocks.containsKey(name)){
            throw new IllegalArgumentException("Share with this name already exists");
        }
        System.out.println(name + " " + s);
        stocks.put(name, s);
        tableData.add(s);
    }

    /**
     *
     * @return all the data from the table using the TableFormattedItem interface
     */
    public ObservableList<TableFormattedItem> getTableData() {
        return tableData;
    }

    /**
     * This method saves the current folio to storage.
     * @param path to where the folio will be stored
     * @throws IOException if there is an error with the path
     */
    public void saveToStorage(String path) throws IOException {
        try(FileOutputStream out = new FileOutputStream(path)){
            ObjectOutputStream stream = new ObjectOutputStream(out);
            this.writeExternal(stream);
            stream.flush();
            stream.close();
        }
    }

    /**
     * This method is for loading a folio in from storage
     * @param path of where the folio is coming from
     * @return the folio
     * @throws IOException if there is an error with the path
     * @throws ClassNotFoundException if there is an error when recreating the folio
     */
    public static Folio loadFromStorage(String path) throws IOException, ClassNotFoundException {
        try (FileInputStream in = new FileInputStream(path)){
            ObjectInputStream stream = new ObjectInputStream(in);
            Folio f = new Folio("");
            f.readExternal(stream);
            stream.close();
            return f;
        }
    }

    /**
     * This method removes a stock from the users folio
     * <p>
     *     It will run through the watches list and remove the stock from that.
     *     Then it will remove the stock from the stock hashmap and tableData
     * </p>
     * @param s The data that is in the table
     */
    public void removeStock(TableFormattedItem s) {
        for (int i =0;i<watches.size();i++){
            if(watches.get(i).getTarget().getLongName().equals(s.getLongName())){
                watches.remove(i);
                i--;
            }
        }
        stocks.remove(s.getLongName());
        tableData.remove(s);
    }

    /**
     * This runs through all the stocks in the hashmap adding up the value of them.
     * @return the total value of the folio
     */
    public double getFolioValue() {
        double value = 0;
        for (HashMap.Entry<String, TableFormattedItem> entry : stocks.entrySet()) {
            TableFormattedItem s = entry.getValue();
            value += s.getNewValue() * s.getQuantity();
        }
        return value;
    }

    /**
     * This runs through all the stocks in the hashmap and adds up their profit.
     * @return the total profit of the folio
     */
    public double getFolioProfit() {
        double value = 0;
        for (HashMap.Entry<String, TableFormattedItem> entry : stocks.entrySet()) {
            TableFormattedItem s = entry.getValue();
            value += s.getProfit();
        }
        return value;
    }

    /**
     * This method increases the number of shares the folio has in a particular stock
     * @param name of the stock
     * @param i The number of shares to add to the current shares in the stock
     */
    public void increaseShares(String name, int i) {
        TableFormattedItem stock = stocks.get(name);
        int amount = stock.getQuantity() + i;
        stock.setQuantity(amount);
    }

    /**
     * This method decreases the number of shares the folio has in a particular stock
     * @param name of the stock
     * @param i The number of shares to subtract to the current shares in the stock
     * @throws IllegalArgumentException if the user is trying to remove more shares than they currently have in the stock
     */
    public void decreaseShares(String name, int i) throws IllegalArgumentException {
        TableFormattedItem stock = stocks.get(name);
        if (stock.getQuantity() < i) {
            throw new IllegalArgumentException("You have less shares than you are trying to remove");
        } else {
            int amount = stock.getQuantity() - i;
            stock.setQuantity(amount);
        }
    }

    /**
     * Returns the number of shares in a stock
     * @param name of the stock
     * @return the number of shares in that stock
     */
    public int getShares(String name) {
        TableFormattedItem stock = stocks.get(name);
        return stock.getQuantity();
    }

    /**
     * This returns the hashmap(Name, Object) of all the stocks
     * @return a hashmap(Name, Object) of all the stocks
     */
    public HashMap<String, TableFormattedItem> getStocks() {
        return stocks;
    }

    /**
     * Gets the name of the folio
     * @return name of the folio
     */
    public String getName() {
        return name;
    }

    /**
     * Changes the name of the folio
     * @param name of the folio
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Changes the name of one of the stocks.
     * <p>
     *     It runs through the hashmap to make sure the new name is also unique.
     * </p>
     * @param current name of the folio
     * @param after new name of the folio
     * @throws IllegalArgumentException is thrown if the new name of the stock is already taken.
     */
    public void changeStockName(String current, String after) throws IllegalArgumentException {
        if (!this.getStocks().containsKey(after)) {
            TableFormattedItem item = this.getStocks().get(current);
            this.getStocks().remove(current);
            item.setLongName(after);
            this.getStocks().put(after, item);
        } else {
            throw new IllegalArgumentException("Name already in HashMap");
        }
    }

    /**
     * Allows the folio object to be serialized so it can be written to file
     * @param objectOutput
     * @throws IOException
     */
    @Override
    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeUTF(this.name);
        objectOutput.writeObject(new ArrayList<>(this.watchData));
        objectOutput.writeObject(new ArrayList<>(this.tableData));
        objectOutput.writeObject(this.stocks);
        objectOutput.writeObject(this.watches);
    }

    public ObservableList<IWatch> getWatchData() {
        return watchData;
    }

    /**
     * Un-serializes an file so that we can add it to the program.
     * @param objectInput
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.name = objectInput.readUTF();
        this.watchData = FXCollections.observableArrayList((ArrayList<IWatch>) objectInput.readObject());
        this.tableData = FXCollections.observableArrayList((ArrayList<TableFormattedItem>) objectInput.readObject());
        this.stocks = (HashMap<String, TableFormattedItem>) objectInput.readObject();
        this.watches = (List<IWatch>) objectInput.readObject();
    }
}

