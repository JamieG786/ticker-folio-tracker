package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.Math.abs;

/**
 * This is a class for all the individual stocks.
 * <p>
 * These objects are handled in the controller through the hashmap in the folio class.
 * </p>
 */
public class Stock implements Serializable, UpdateStockInterface, TableFormattedItem {
    private String symbol;
    private String name;
    private int shares;
    private double currentPrice;
    private double purchasePrice;
    private double low;
    private double high;
    private static final long serialVersionUID=28L;

    /**
     * Creates a new stock for a folio.
     * @param s The ticker symbol of the stock
     * @param n The users desired name of the stock
     * @param shares The number of shares the user has in the stock
     * @throws NoSuchTickerException is thrown if the website cannot find the stock based on the symbol
     * @throws WebsiteDataException is thrown if there is an error retrieving data from the website
     */
    public Stock(String s, String n, int shares) throws NoSuchTickerException, WebsiteDataException {
        symbol = s;
        name = n;
        this.shares = shares;
        updatePrice(new StrathQuoteServer());
        this.purchasePrice= currentPrice;
        high = currentPrice;
        low = currentPrice;
    }

    //Can this be removed?
    public Stock(String symbol, String name, int shares, double purchasePrice,
                 double low, double high) throws NoSuchTickerException, WebsiteDataException {
        this.symbol = symbol;
        this.name = name;
        this.shares = shares;
        this.low = low;
        this.high = high;
        updatePrice(new StrathQuoteServer());
        this.purchasePrice = purchasePrice;
    }

    /**
     * This method updates prices of the stock.
     * <p>
     *     It will also update the high and low values if the new prices is higher or lower respectively.
     * </p>
     * @param server
     * @throws NoSuchTickerException is thrown if it cannot find the Ticker
     * @throws WebsiteDataException is thrown if there is an issue with the website
     */
    public void updatePrice(StrathQuoteServer server) throws NoSuchTickerException, WebsiteDataException {
        currentPrice =  Double.parseDouble(server.getLastValue(symbol));
        if (currentPrice<low){
            low=currentPrice;
        }
        if (currentPrice>high){
            high=currentPrice;
        }
    }

    /**
     * This method will change the symbol of a stock.
     * <p>
     *    It will also retrieve a new price for the stock from the API as it is no longer the same stock.
     * </p>
     * @param symbol The new ticker symbol
     * @throws NoSuchTickerException is thrown is the website cannot find a stock based on the symbol
     * @throws WebsiteDataException is thrown is there is an error with the website
     */
    public void setSymbol(String symbol) throws NoSuchTickerException, WebsiteDataException {
        this.symbol = symbol;
        updatePrice(new StrathQuoteServer());
    }

    @Override
    public void setLongName(String name) { this.name = name; }

    @Override
    public void setQuantity(int newQuantity) {
        if(this.shares<newQuantity){
            purchasePrice= (this.shares*purchasePrice+currentPrice*abs(newQuantity-this.shares))/newQuantity;
        }
        this.shares = newQuantity;
    }

    @Override
    public void setOldValue(double newPrice) { this.purchasePrice = newPrice; }

    @Override
    public String getLongName() { return this.name; }

    @Override
    public String getShortName() { return this.symbol; }

    @Override
    public double getNewValue() { return this.currentPrice; }

    @Override
    public double getOldValue() { return this.purchasePrice; }

    public double getLow() {
        return low;
    }

    public double getHigh() {
        return high;
    }

    @Override
    public int getQuantity() { return this.shares; }

    /**
     * This gets the profit for a stock.
     * <p>It does this by subtracting current price from purchase price</p>
     * @return the profit for the stock.
     */
    @Override
    public double getProfit() {
        BigDecimal a =new BigDecimal((this.currentPrice-this.purchasePrice) * this.shares);
        a=a.setScale(3, RoundingMode.HALF_EVEN);
        return  a.doubleValue();
    }
}
