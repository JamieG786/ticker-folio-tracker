package model;

/**
 * Interface to generalize the properties the table in the view needs
 */
public interface TableFormattedItem {
    void setQuantity(int newQuantity);
    void setOldValue(double oldValue);
    void setLongName(String name);
    String getLongName();
    String getShortName();
    double getNewValue();
    double getOldValue();
    int getQuantity();
    double getProfit();
    double getHigh();
    double getLow();
}
