package model;

import java.util.HashMap;

/**
 * This is the class for the model. This will we used by the controller to connect the backend to the view.
 */
public class Model {
    private HashMap<String, Folio> folios = new HashMap<>();
    private String currentFolioKey;

    /**
     * This method is for adding a Folio.
     * <p>
     *  It will check if there is already a folio with this name and if so it will throw an exception.
     *  If it passes validation it will then add the folio to the hashmap.
     * </p>
     * @param name of the folio
     * @return The folio object
     * @throws IllegalArgumentException is thrown if there is already a folio with this name
     */
    public Folio addFolio(String name) throws IllegalArgumentException {
        Folio f = new Folio(name);
        if(folios.containsKey(name)){
            throw new IllegalArgumentException("Folio with this name already exists");
        }
        folios.put(name, f);
        currentFolioKey = name;
        return f;
    }

    /**
     * This method will switch the current folioKey that the model is dealing with.
     * <p>
     * It will make sure the folio is within the hashmap and then change the currentFolioKey to the name of the folio that has been passed in.
     * </p>
     * @param name of the folio to change to
     * @throws IllegalArgumentException is thrown if the folio does not exist.
     */
    public void switchCurrentFolio (String name) throws IllegalArgumentException {
        if (folios.containsKey(name)) {
            currentFolioKey = name;
        } else {
            throw new IllegalArgumentException("Folio does not exist");
        }
    }

    /**
     * This will remove a folio from the hashmap
     * @param f The folio to be removed
     */
    public void removeFolio(Folio f) {
        folios.remove(f.getName());
    }

    /**
     * This will get the all the stocks within a folio
     * @param f The folio to get the stocks from
     * @return a hashmap of all the stocks within the given folio
     */
    public HashMap<String, TableFormattedItem> getStocks(Folio f) {
        return f.getStocks();
    }

    /**
     * Get the name of a folio
     * @param f The folio to get the name from
     * @return the given folios name
     */
    public String getFolioName(Folio f) {
        return f.getName();
    }

    /**
     * Get the value of a folio
     * @param f The folio to get the value of
     * @return the given folios valye
     */
    public double getFolioValue(Folio f) {
        return f.getFolioValue();
    }

    public Folio getCurrentFolio() {
        return folios.get(currentFolioKey);
    }

    /**
     * This method add shares to a stock within a folio.
     * @param f The folio that the stock is in
     * @param name of the stock
     * @param i Number of shares to add to the stock
     */
    public void addShares(Folio f, String name, int i) {
        f.increaseShares(name, i);
    }

    /**
     * This method removes shares to a stock within a folio.
     * @param f The folio that the stock is in
     * @param name of the stock
     * @param i Number of shares to remove from the stock
     * @throws IllegalArgumentException is thrown if the number of shares to remove is less than the number of shares currently.
     */
    public void removeShares(Folio f, String name, int i) throws IllegalArgumentException {
        f.decreaseShares(name, i);
    }

    /**
     * This is the method to refresh the prices of the stocks of every folio.
     * <p>
     *     It will run through every stock within every folio and get the price.
     *     Then it will pass the new price into the update price function.
     * </p>
     * @throws WebsiteDataException is thrown if there is an error with the website.
     * @throws NoSuchTickerException is thrown if the website cant find a stock with that ticker symbol.
     */
    public void refreshStocks() throws WebsiteDataException, NoSuchTickerException {
        for (HashMap.Entry<String, Folio> f : folios.entrySet()) {
            HashMap<String, TableFormattedItem> stocks = new HashMap<>(f.getValue().getStocks());
            for(HashMap.Entry<String, TableFormattedItem> s : stocks.entrySet()) {
                TableFormattedItem stock = s.getValue();
                ((UpdateStockInterface) stock).updatePrice(new StrathQuoteServer());
                f.getValue().checkWatches();
            }
            f.getValue().checkWatches();
        }
    }

    /**
     * This method will get the profit of a folio.
     * @param f The folio to get the profit of
     * @return the profit of the given folio
     */
    public double currentFolioProfit(Folio f) {
        return f.getFolioProfit();
    }
}
