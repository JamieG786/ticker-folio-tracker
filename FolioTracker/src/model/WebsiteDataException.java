package model;

public class WebsiteDataException extends Exception {
    WebsiteDataException() {
    }

    WebsiteDataException(String s) {
        super(s);
    }
}
