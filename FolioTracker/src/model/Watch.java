package model;

/**
 * This is the class that deals with watched created by the user.
 */
public class Watch implements IWatch {
    private double triggerValue;
    private int amount;
    private int action;
    private Stock target;
    private String name;

    /**
     * Creates a new watch.
     * @param name
     * @param target
     * @param triggerValue
     * @param amount
     * @param action
     */
    public Watch(String name,Stock target,double triggerValue, int amount, int action) {
        this.target=target;
        this.name=name;
        this.triggerValue = triggerValue;
        this.amount = amount;
        this.action = action;
    }

    @Override
    public double getTriggerValue() {
        return triggerValue;
    }

    @Override
    public int getAmount() {
        return amount;
    }

    @Override
    public int getAction() {
        return action;
    }

    @Override
    public Stock getTarget() {
        return target;
    }

    @Override
    public String getName() {
        return name;
    }
}
