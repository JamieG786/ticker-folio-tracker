import model.Folio;
import model.StrathQuoteServer;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class testClass {

    public static void main(String[] args) {
        try {
            Folio t=new Folio("folio");
            t.addStock("TXT","test",20);
            t.saveToStorage("file.filo");
            Folio l = Folio.loadFromStorage("file.filo");
            assert (l.equals(t));

            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader reader = new BufferedReader(isr);
            String line = null;
            System.out.print("Input a ticker symbol: ");
            while ((line = reader.readLine()) != null) {
                String str = (new StrathQuoteServer()).getLastValue(line);
                System.out.println(line + " has a stock value of " + str);
                System.out.print("Input a ticker symbol: ");
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}