package view;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.IWatch;
import view.components.WatchAdderBox;

/**
 * This creates a pop up for the users to create new watches
 */
public class WatchesView extends Stage {
    private WatchAdderBox watchAdderBox;
    private TableView<IWatch> watchTable;

    /**
     * this method sets up the view for the user
     * @param watches List of all the watches
     */
    public WatchesView(ObservableList<IWatch> watches){
        setUpTable(watches);
        this.watchAdderBox=new WatchAdderBox(new BorderStroke(
                Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.DEFAULT_WIDTHS
        ));
        VBox topBox = new VBox(watchAdderBox,watchTable);
        this.setScene(new Scene(topBox));

    }

    /**
     * This method sets up the table to display the watches
     * @param watches List of all the watches
     */
    private void setUpTable(ObservableList<IWatch> watches) {
        this.watchTable=new TableView<>();
        TableColumn<IWatch, String> name;
        TableColumn<IWatch, String> type;
        TableColumn<IWatch, String> price;
        TableColumn<IWatch, String> amount;
        TableColumn<IWatch, String> targetName;

        name = new TableColumn<>("Watch Name");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));

        type = new TableColumn<>("Type");
        type.setCellValueFactory(data->{
            IWatch w = data.getValue();
            if (w.getAction()==IWatch.BUY){
                return new SimpleStringProperty("Buy");
            }
            else {
                return new SimpleStringProperty("Sell");
            }
        });
        price = new TableColumn<>("Trigger Price");
        price.setCellValueFactory(new PropertyValueFactory<>("triggerValue"));

        amount = new TableColumn<>("Amount to use");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        targetName = new TableColumn<>("Stock Name");
        targetName.setCellValueFactory(data->{
            IWatch w = data.getValue();
            return new SimpleStringProperty(w.getTarget().getLongName());
        });

        this.watchTable.getColumns().addAll(name,amount,price,targetName,type);

        this.watchTable.setItems(watches);
    }

    public WatchAdderBox getWatchAdderBox() {
        return watchAdderBox;
    }

    public TableView<IWatch> getWatchTable() {
        return watchTable;
    }
}
