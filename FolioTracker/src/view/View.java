package view;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import view.components.ActionBar;
import view.components.MainMenuBar;
import view.components.StatusBar;
import view.components.TablesPane;

import java.util.Optional;

/**
 * Main tunnel into the view so that controller can access components
 */
public class View extends Stage {

    private MainMenuBar menuBar;
    private StatusBar statusBar;
    public ActionBar actionBar;
    private TablesPane tablesPane;

    public enum FileAccess {
        LOAD,
        SAVE
    }

    public View() {
        VBox root = new VBox();
        this.menuBar = new MainMenuBar();
        this.statusBar = new StatusBar();
        this.actionBar = new ActionBar();
        this.tablesPane = new TablesPane();

        root.getChildren().addAll(menuBar, actionBar, tablesPane, statusBar);

        this.setTitle("Folio Tracker");
        Rectangle2D screenBounds = Screen.getPrimary().getBounds();
        this.setScene(new Scene(root, screenBounds.getWidth()*0.6, screenBounds.getHeight()*0.6));
    }

    /**
     * Takes in an error from the caller that will be displayed in
     * a dialogue box.
     * @param error
     */
    public void displayBasicError(String error) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(null);
        alert.setContentText(error);
        alert.showAndWait();
    }

    /**
     * Displays a dialogue box that allows the user to confirm whether
     * or not they actually wish to remove the currently selected stock
     * @param stockName
     * @return
     */
    public boolean displayStockRemoveDialog(String stockName) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Remove");
        alert.setHeaderText("Remove Stock");
        alert.setContentText("Are you sure you wish to remove " + stockName + " from the table?");

        Optional<ButtonType> choice = alert.showAndWait();
        return choice.get() == ButtonType.OK;
    }

    /**
     * Displays a dialogue box that allows the user to confirm whether
     * or not they actually wish to remove the currently selected watch
     * @param watchName
     * @return
     */
    public boolean displayWatchRemoveDialog(String watchName) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Remove");
        alert.setHeaderText("Remove Watch");
        alert.setContentText("Are you sure you wish to remove " + watchName + " from the folio?");

        Optional<ButtonType> choice = alert.showAndWait();
        return choice.get() == ButtonType.OK;
    }


    /**
     * Displays a fileChooser in either a LOAD or a SAVE state so that the user
     * can save/load a folio. Returns the path of the file location so that
     * the file can be saved at a later point in the program
     * @param usage
     * @param name
     * @return
     */
    public String getFilePath(FileAccess usage, String name) {
        String path = "";
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Folio files (*.folio)", "*.folio"));

        if (usage == FileAccess.LOAD) {
            path = fileChooser.showOpenDialog(this).getAbsolutePath();
        } else if (usage == FileAccess.SAVE) {
            fileChooser.setInitialFileName(name);
            path = fileChooser.showSaveDialog(this).getAbsolutePath();
        }

        return path;
    }

    public MainMenuBar getMenuBar() {
        return menuBar;
    }

    public StatusBar getStatusBar() {
        return statusBar;
    }

    public ActionBar getActionBar() {
        return actionBar;
    }

    public TablesPane getTablesPane() { return tablesPane; }
}
