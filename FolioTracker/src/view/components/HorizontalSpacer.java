package view.components;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 * This is a node that can be inserted in the view to space
 * nodes apart
 */
public class HorizontalSpacer extends Region {
    public HorizontalSpacer() {
        HBox.setHgrow(this, Priority.ALWAYS);
        this.setMinWidth(10);
    }
}
