package view.components;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class WatchAdderBox extends VBox {

    private final ChoiceBox<String> choiceBox;
    private Button removeButton, addButton;
    private TextField nameText, stockText, priceText, amountText;

    public WatchAdderBox(BorderStroke borderStyle) {
        this.setPadding(new Insets(10, 10, 10, 0));
        this.setBorder(new Border(borderStyle));

        this.removeButton = new Button("Remove Watch");
        this.addButton = new Button("Add");

        this.nameText = new TextField();
        this.nameText.setPromptText("Name");
        this.nameText.setPrefColumnCount(10);

        this.amountText = new TextField();
        this.amountText.setPromptText("Amount");
        this.amountText.setPrefColumnCount(10);

        this.stockText = new TextField();
        this.stockText.setPromptText("Stock Name");
        this.stockText.setPrefColumnCount(10);

        this.choiceBox= new ChoiceBox<>(FXCollections.observableArrayList("Sell","Buy"));
        this.choiceBox.setValue("Sell");

        this.priceText = new TextField();
        this.priceText.setPromptText("Price");
        this.priceText.setPrefColumnCount(5);

        HBox topLayer = new HBox();
        Insets PADDING_INSETS = new Insets(5);
        topLayer.setPadding(PADDING_INSETS);
        topLayer.setSpacing(10);
        topLayer.getChildren().addAll(
                this.nameText, this.choiceBox, this.stockText, this.amountText,
                this.priceText
        );

        HBox bottomLayer = new HBox();
        bottomLayer.setPadding(PADDING_INSETS);
        bottomLayer.setSpacing(10);
        bottomLayer.getChildren().addAll(removeButton, this.addButton);

        this.getChildren().addAll(topLayer, bottomLayer);
    }

    /**
     * This empties the text fields in the WatchAdderBox.
     * This would be used when an item has been added and the
     * content is no longer relevant.
     */
    public void emptyFields() {
        this.nameText.setText("");
        this.stockText.setText("");
        this.priceText.setText("");
        this.amountText.setText("");
    }

    public Button getRemoveButton() { return removeButton; }
    public Button getAddButton() { return addButton; }

    public ChoiceBox<String> getChoiceBox() {
        return choiceBox;
    }

    public TextField getNameText() {
        return nameText;
    }

    public TextField getStockText() {
        return stockText;
    }

    public TextField getPriceText() {
        return priceText;
    }

    public TextField getAmountText() {
        return amountText;
    }
}


