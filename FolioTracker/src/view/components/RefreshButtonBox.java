package view.components;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.HBox;

/**
 * Refresh button box that holds a button to allow the user
 * to refresh the table manually
 */
public class RefreshButtonBox extends HBox {

    private Button refreshButton;
    private Button editWatchesButton;

    public RefreshButtonBox(BorderStroke borderStyle) {
        this.setPadding(new Insets(10, 0, 10, 10));
        this.setBorder(new Border(borderStyle));
        this.refreshButton = new Button("Refresh");
        this.editWatchesButton=new Button("Edit Watches");
        this.setSpacing(10);

        this.getChildren().addAll(refreshButton,editWatchesButton);
    }

    public Button getRefreshButton() { return refreshButton; }
    public Button getEditWatchesButton() { return editWatchesButton; }

}
