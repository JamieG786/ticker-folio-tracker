package view.components;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;

/**
 * Container that contains containers of buttons that allow the
 * user to modify the items in the table
 */
public class ActionBar extends GridPane {
    private StockModifierBox stockModifierBox;
    private RefreshButtonBox refreshButtonBox;

    public ActionBar() {
        final BorderStroke BORDER_STYLE = new BorderStroke(
                Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.DEFAULT_WIDTHS
        );

        this.stockModifierBox = new StockModifierBox(BORDER_STYLE);
        this.refreshButtonBox = new RefreshButtonBox(BORDER_STYLE);

        this.getColumnConstraints().addAll(new ColumnConstraints(), new ColumnConstraints());

        this.getColumnConstraints().get(0).setPercentWidth(50);
        this.add(this.refreshButtonBox, 0, 0);

        this.getColumnConstraints().get(1).setPercentWidth(50);
        this.add(this.stockModifierBox, 1, 0);
    }

    public StockModifierBox getStockModifierBox() { return stockModifierBox; }

    public RefreshButtonBox getRefreshButtonBox() { return refreshButtonBox; }
}
