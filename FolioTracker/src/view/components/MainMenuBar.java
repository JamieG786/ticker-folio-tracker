package view.components;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * Menu bar that allows the user to access additional options
 * such as the ability to save and reload a portfolio.
 */
public class MainMenuBar extends MenuBar {

    private MenuItem newPort, savePort, saveAll, loadPort, exit;

    public MainMenuBar() {
        this.newPort = new MenuItem("New Portfolio");
        this.savePort = new MenuItem("Save Portfolio");
        this.loadPort = new MenuItem("Load Portfolio");
        this.exit = new MenuItem("Exit");

        Menu menuFile = new Menu("File");
        menuFile.getItems().addAll(
                newPort, savePort, loadPort, exit
        );
        this.getMenus().add(menuFile);
    }

    public MenuItem getNewPort() {
        return newPort;
    }

    public MenuItem getSavePort() {
        return savePort;
    }

    public MenuItem getLoadPort() {
        return loadPort;
    }

    public MenuItem getExit() {
        return exit;
    }
}
