package view.components;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * Status bar that holds statistics about the folios contained in
 * the program
 */
public class StatusBar extends HBox {

    private Label portVal;
    private Label overallProfit;
    private Label lastRefresh;
    private Button folioValue;

    public StatusBar () {
        this.portVal = new Label();
        this.lastRefresh = new Label();
        this.overallProfit = new Label();
        this.folioValue = new Button("Folio Value");
        HorizontalSpacer th = new HorizontalSpacer();
        th.setMaxWidth(15);

        this.getChildren().addAll(
               // folioValue,
                new Label("Portfolio Value : "), this.portVal, th,
                new Label("Overall Profit : "), this.overallProfit,
                new HorizontalSpacer(),
                new Label("Last Refresh : "), this.lastRefresh
        );
    }

    public Label getPortVal() { return portVal; }

    public Label getOverallProfit() { return overallProfit; }

    public Label getLastRefresh() { return lastRefresh; }

    public Button getFolioValue() { return folioValue; }

}
