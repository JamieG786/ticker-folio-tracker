package view.components;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import model.TableFormattedItem;

/**
 * This class combines both tables and tabs to display
 * the table within a tab
 */
public class TableTab extends Tab {

    private TableView<TableFormattedItem> table;
    private String name;

    public TableTab(String name, ObservableList<TableFormattedItem> items) {
        this.name=name;
        this.setText(name);
        this.setUpTab(items);
        table.refresh();
    }

    /**
     * Sets up a table based on the properties of a TableFormattedItem
     * It also adds columns based off of calculations of these values
     * @param items
     */
    public void setUpTab(ObservableList<TableFormattedItem> items) {
        this.table = new TableView<>();
        TableColumn<TableFormattedItem, String> symbol;
        TableColumn<TableFormattedItem, String> longName;
        TableColumn<TableFormattedItem, String> quantity;
        TableColumn<TableFormattedItem, String> oldValue;
        TableColumn<TableFormattedItem, String> newValue;
        TableColumn<TableFormattedItem, String> overAllValue;
        TableColumn<TableFormattedItem, String> profit;
        TableColumn<TableFormattedItem, String> low;
        TableColumn<TableFormattedItem, String> high;

        symbol = new TableColumn<>("Ticker Symbol");
        symbol.setCellValueFactory(new PropertyValueFactory<>("shortName"));

        longName = new TableColumn<>("Stock Name");
        longName.setCellValueFactory(new PropertyValueFactory<>("longName"));

        quantity = new TableColumn<>("Number of Shares");
        quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        oldValue = new TableColumn<>("Purchase Price/Share");
        oldValue.setCellValueFactory(new PropertyValueFactory<>("oldValue"));

        newValue = new TableColumn<>("Current Price/Share");
        newValue.setCellValueFactory(new PropertyValueFactory<>("newValue"));

        high = new TableColumn<>("High");
        high.setCellValueFactory(new PropertyValueFactory<>("high"));

        low = new TableColumn<>("Low");
        low.setCellValueFactory(new PropertyValueFactory<>("low"));

        overAllValue = new TableColumn<>("Value");
        overAllValue.setCellValueFactory(data -> {
            TableFormattedItem item = data.getValue();
            double value = item.getQuantity() * item.getNewValue();
            return new SimpleStringProperty(""+(value));
        });

        profit = new TableColumn<>("Profit");
        profit.setCellFactory(tableFormattedItemTableFormattedItemTableColumn -> new TableCell<>(){
            @Override
            protected void updateItem(String s, boolean b) {
                super.updateItem(s, b);
                if(this.getTableRow()==null||b){
                    return;
                }
                TableFormattedItem data = this.getTableRow().getItem();
                if(data==null){
                    return;
                }
                double profit=data.getProfit();
                this.setText(profit<0?""+profit:"+"+profit);
                if (profit<0) {
                    String color = "#FF3B36";
                    setStyle("-fx-background-color: " + color + ";");
                } else if (profit<=0.001&&profit>=-0.001) {
                    this.setStyle(null);
                } else {
                    String color = "#8FE76E";
                    setStyle("-fx-background-color: " + color + ";");
                }
            }
        });

        this.table.getColumns().addAll(symbol, longName, quantity,
                oldValue, newValue, overAllValue, profit,high,low);

        table.setItems(items);
        this.setContent(this.table);
    }

    public TableFormattedItem getSelectedShare() {
        return this.table.getSelectionModel().getSelectedItem();
    }

    public TableView<TableFormattedItem> getTable() {
        return this.table;
    }

    public String getName() {
        return name;
    }
}
