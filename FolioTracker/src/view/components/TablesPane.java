package view.components;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import model.Folio;

/**
 * Container of Tables in tab form
 */
public class TablesPane extends TabPane {

    public TableTab getTab(String name){
        for (Tab t:getTabs()){
            if(((TableTab)t).getName().equals(name)){
                return (TableTab) t;
            }
        }
        return null;
    }

    /**
     * Allows helper function so that a new folio can
     * be represented in the program.
     * @param folio
     */
    public void createFolioTab(Folio folio){
        this.getTabs().add(new TableTab(folio.getName(),folio.getTableData()));
    }

}
