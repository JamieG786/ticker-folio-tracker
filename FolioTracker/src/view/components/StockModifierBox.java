package view.components;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

/**
 * Container to hold buttons and text boxes that allow the user
 * to modify the table
 */
public class StockModifierBox extends VBox {

    private Button removeButton, editButton, addButton;
    private TextField nameText, tickerText, quantityText;

    public StockModifierBox(BorderStroke borderStyle) {
        this.setPadding(new Insets(10, 10, 10, 0));
        this.setBorder(new Border(borderStyle));

        this.removeButton = new Button("Remove Stock");
        this.editButton = new Button("Edit Stock");
        this.addButton = new Button("Add");

        this.nameText = new TextField();
        this.nameText.setPromptText("Name");
        this.nameText.setPrefColumnCount(10);

        this.tickerText = new TextField();
        this.tickerText.setPromptText("Enter Ticker Here");
        this.tickerText.setPrefColumnCount(10);

        this.quantityText = new TextField();
        this.quantityText.setPromptText("Quantity");
        this.quantityText.setPrefColumnCount(5);

        HBox topLayer = new HBox();
        HorizontalSpacer ths = new HorizontalSpacer(), th = new HorizontalSpacer();
        ths.setMaxWidth(15);
        th.setMaxWidth(15);
        Insets PADDING_INSETS = new Insets(5);
        topLayer.setPadding(PADDING_INSETS);
        topLayer.getChildren().addAll(
                this.nameText, ths, this.tickerText, th, this.quantityText, new HorizontalSpacer(), this.addButton
        );

        HBox bottomLayer = new HBox();
        bottomLayer.setPadding(PADDING_INSETS);
        bottomLayer.getChildren().addAll(removeButton, new HorizontalSpacer(), editButton);

        this.getChildren().addAll(topLayer, bottomLayer);
    }

    /**
     * This empties the text fields in the StockModifierBox.
     * This would be used when an item has been added and the
     * content is no longer relevant.
     */
    public void emptyFields() {
        this.nameText.setText("");
        this.tickerText.setText("");
        this.quantityText.setText("");
    }

    public Button getRemoveButton() { return removeButton; }
    public Button getEditButton() { return editButton; }
    public Button getAddButton() { return addButton; }

    public TextField getTickerText() { return tickerText; }
    public TextField getQuantityText() { return quantityText; }
    public TextField getNameText() { return nameText;}
}
