package view;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Produces a dialogue box that allows the user to name a new portfolio
 */
public class CreateFolio extends Stage{
    private Button create=new Button("Create");
    private TextField name = new TextField();
    private Stage s = new Stage();
    private Label label = new Label("Please enter a name for the folio:");

    public CreateFolio() {
        VBox root = new VBox();
        this.name.setPromptText("Name");
        root.getChildren().addAll(label, name, create);
        Rectangle2D screenBounds = Screen.getPrimary().getBounds();
        Scene popup = new Scene(root, screenBounds.getWidth() * 0.2, screenBounds.getHeight() * 0.1);
        System.out.println(create);
        s.setTitle("Create new folio");
        s.setScene(popup);
    }

    public void showFolioScene() {
        s.show();
    }

    public void hideFolioScene() {
        s.hide();
    }

    public Button getCreate() { return create;}

    public String getName() { return name.getText();}

    public void reset() {
        name.setText("");
    }

}
