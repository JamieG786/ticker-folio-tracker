package view;

import javafx.stage.StageStyle;
import model.TableFormattedItem;
import view.components.HorizontalSpacer;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Produces a temporary UI component that will display information
 * about the currently selected stock so that the user can edit them
 */
public class EditStockMenu extends Stage {

    private TextField nameText, quantityText, purchaseVal;
    private Button acceptButton, cancelButton;

    public EditStockMenu(TableFormattedItem item) {
        VBox root = new VBox();
        root.setPadding(new Insets(10));

        this.nameText = new TextField(item.getLongName());
        this.quantityText = new TextField(item.getQuantity()+"");
        this.purchaseVal = new TextField(item.getOldValue()+"");

        this.acceptButton = new Button("Accept");
        this.cancelButton = new Button("Cancel");
        this.setTitle("Edit Share");

        Label header = new Label("Edit Share : " + item.getLongName());
        header.setFont(new Font(16));

        root.getChildren().addAll(header);
        root.getChildren().add(new HBox(new Label("Current Value : " + item.getNewValue())));
        root.getChildren().add(new HBox(new Label("Quantity : "), this.quantityText));
        root.getChildren().add(new HBox(new Label("Purchase Value : "), this.purchaseVal));
        root.getChildren().add(new HBox(new Label("Name : "), this.nameText));
        root.getChildren().add(new HBox(this.acceptButton, new HorizontalSpacer(), this.cancelButton));

        this.initStyle(StageStyle.UTILITY);

        this.setResizable(false);
        this.setScene(new Scene(root));
    }

    public TextField getNameText() {
        return nameText;
    }

    public TextField getQuantityText() {
        return quantityText;
    }

    public TextField getPurchaseVal() {
        return purchaseVal;
    }

    public Button getAcceptButton() {
        return acceptButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }
}
