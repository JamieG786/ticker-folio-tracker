# Ticker Folio Tracker

A Java project created by Jamie Greenaway, Paul Hutchison, Valentin Petrov, Ross Gribben and David Lewis for the class CS308 at the University of Strathclyde.

The requested artifacts can be found at the following directories:

source code  - FolioTracker/src/..
test code    - FolioTracker/Test/..
runnable jar - FolioTracker/Jar/cs308_2.jar

The jar can be copied and ran from anywhere.

Overall Design               - /docs/Overall-Design.pdf
Specification of API         - /docs/Specification-of-API.pdf
Testing Rational             - /docs/Testing-Rational.pdf
UMLS                         - /docs/UMLS

In addition, full JavaDocs are available at:
    /docs/Javadocs/index.html
 